# PRÀCTICA  UF-1
## Simulació d’un compressor – descompressor 

---

La pràctica consisteix a realitzar la simulació d’un compressor i descompressor de text amb c. Es demana un text  per teclat, i a cada caràcter del text se li assigna un codi de bits. El caràcter més repetit tindrà el codi més petit, i el menys repetit, tindrà el codi més gran. Un cop comprimit el text, es demana per teclat introduir el text comprimit, i a continuació, s’imprimeix per pantalla el text descomprimit. 

![](Imatges/Imatge6.jpg)

---

### Índex

- ##### [Declaracions i Inicialitzacions.](#id1)<br>
- ##### [Omplir els vectors](#id2)<br>
- ##### [Ordenar els vectors](#id3)<br>
- ##### [Imprimir els resultats](#id4)<br>
- ##### [Descompactació](#id5)<br>

---

<div id='id1' />

### Declaracions i inicialitzacions

El primer pas que he realitzat ha sigut declarar tant les constants com les variables que aniré utilitzant durant el programa. <br>

```c
#define BB while (getchar()!='\n')
#define MAX_TEXT 100
#define MAX_REP 100
#define MAX_CAR 100
#define MAX_CODIS 29
#define MAX_DIGITS 5
#define PERCENTATGE_COMPACTACIO ((((float)iTxt * 8) - (float)bit)/((float)iTxt*8)*100)
#define MAX_TEXT_COMP 300
```

```c
// Declaració de variables
int iTxt, iCar, iRep, iCod, iBit, iCodTmp; // Index
int i, max, aux; // Ordenar caràcters i repeticions
int bit; // Numero de bits que hi ha al text compactat
int rep[MAX_REP+1]; // Numero de cops que es repeteix cada caràcter
char text[MAX_TEXT+1]; // Text a compactar, introduit per l'usuari
char textComp[MAX_TEXT_COMP+1]; // Text compactat
char car[MAX_CAR+1]; // Caràcters del text descompactat
char auxCar; // Ordenar caràcters. Guarda momentàniament el caràcter per no perdre-l
char codi[MAX_CODIS][ MAX_DIGITS+1]={ "0" , "1" , "00" , "01" , "10" , "11" , "000" ,
"001" , "010" , "011" , "100" , "101" , "110" , "111" , "0000" , "0001" , "0010" ,
"0011" , "0100" , "0101" , "0110" , "0111" , "1000" , "1001" , "1010" , "1100" , "1101" , "1110" , "1111" };
char codiTmp[MAX_DIGITS+1]; // Descompactar. Es copia el codi en aquesta variable per poder-lo comparar amb els codis del vector codi.
```

Seguidament, he inicialitzat l'índex de text i el contador de bits amb 0. Després he inicialitzat el vector de repeticions amb 1 i el vector de caràcters amb *'\0'*. Aquest vector l’he inicialitzat d’aquesta forma perquè així cada cop que el vas omplint amb un caràcter, al següent espai del vector hi haurà un *'\0'*, la qual cosa voldrà dir que sempre trobarà el final del vector, que serà quan es trobi un *'\0'*. <br>

```c
iTxt= 0;
bit= 0;
for (iRep=0; iRep < MAX_REP; iRep++) rep[iRep]=1; // Inicialització de les repeticions amb 1
for (iCar=0; iCar < MAX_CAR; iCar++) car[iCar]='\0'; // Inicialització dels caràcters amb /0
```

A continuació, li demano a l’usuari que entri el text que vulgui compactar per teclat.

```c
printf ("Introdueix el text a codificar: \n");
scanf ("%100[^\n]", text); BB;
```

![](Imatges/Imatge2.jpg)
<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

<div id='id2' />

### Omplir els vectors

Ara que ja tinc el text que l’usuari vol compactar, és necessari omplir el vector de caràcters amb els diferents caràcters i el vector repeticions amb el nombre de cops que es repeteix cada caràcter. 
El programa, el que fa és mirar cada caràcter del text fins que no arribi al final. Aquest caràcter el busca al vector de caràcters. Si el troba, li suma una repetició, en canvi, si arriba al final del vector i no l’ha trobat, l’ha d’afegir. 

```c
// Mentre no final
while (text[iTxt] != '\0') {
    iCar=0;
    iRep=0;
    // Mirar si el caràcter del text és al vector car
    while (car[iCar] != '\0' && car[iCar] != text[iTxt]) {
            iCar++;
            iRep++;
        }
    // Si no hi és, afegir-lo
    // Si hi és, sumar una repetició
    if (car[iCar] == '\0') {
        car[iCar]=text[iTxt];
        iCar++;
    } else rep[iRep]++;
    iTxt++;
} // Fi mentre no final
```

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

<div id='id3' />

### Ordenar els vectors

Un cop omplert els dos vectors, s’han d’ordenar, per tal de poder-li assignar al caràcter més repetit el codi més petit, per tal que la compressió sigui del tot òptima. L’ordenació l’he realitzat amb el mètode de la bombolla. 
El que fa el programa és mirar si el nombre de repeticions del caràcter és mes petit que el nombre de repeticions del següent. Si ho és, s’intercanvien les posicions, si no, no, i passa a mirar el següent caràcter fins arribar al final. Així, el caràcter menys repetit se’n va al final, i ja no el tracta, perquè ja està ordenat. 
I aquesta iteració la fa fins que tots els caràcters queden ordenats de major a menor nombre de repeticions. 

```c
 // Ordenar els caràcters del vector car per nombre de repeticions
max= 0;
while (car[max] != '\0') max++;
for (i=2; i <= max; i++) {
    for (iRep=0 ; iRep <= max-i ; iRep++) {
        // Si el caràcter te menys repeticions que el següent, intercanviar-los
        if (rep[iRep] < rep[iRep+1]) {
            aux=rep[iRep];
            rep[iRep]=rep[iRep+1];
            rep[iRep+1]=aux;
            auxCar=car[iRep];
            car[iRep]=car[iRep+1];
            car[iRep+1]=auxCar;
        }
    }
}
```

---

<div id='id4' />

### Imprimir els resultats de la compactació

El següent pas es imprimir per pantalla el text compactat.
El que fa és buscar cada caràcter del text al vector de caràcters, i imprimir el codi que li correspon dígit a dígit, per poder sumar els dígits, i així calcular l’espai que ocupa el text compactat i el percentatge de compactació. Abans de imprimir cada codi, excepte al principi, imprimeix un dollar per poder separar els codis. 

```c
 iTxt=0;
// Imprimir text compactat
printf ("\nText compactat:\n");
// Mentre no final
while (text[iTxt] != '\0') {
    iCar=0;
    iCod=0;
    while (car[iCar] != '\0') {
        iBit=0;
        // Imprimir el codi del caràcter concret i anar sumant els bits
        if (car[iCar] == text[iTxt]) {
            if (iTxt != 0) printf ("$");
            while (codi[iCod][iBit] != '\0') {
                printf ("%c", codi[iCod][iBit]);
                iBit++, bit++;
            }
            break;
        } else iCar++, iCod++;
    }
    iTxt++;
} // Fi mentre no final
```

![](Imatges/Imatge4.jpg)

A continuació imprimeixo el percentatge de compactació i una taula amb cada caràcter, el nombre de cops que apareix el caràcter i el codi que tè assignat.

```c
// Imprimir el percentatge de compactació
printf ("\n\nBits del text: %ix8 = %i bits\n", iTxt, iTxt*8);
printf ("Bits del text compactat: %i bits", bit);
printf ("\nPercentatge de compactacio: %.2f%%", PERCENTATGE_COMPACTACIO);

iCar=0;
iRep=0;
iCod=0;

// Imprimir la taula de caràcters
printf ("\n\nTaula (caracter - repeticions - codi assignat)\n");
while (car[iCar] != '\0'){
    printf ("'%c'\t'%i repeticio'\t'%s'\n", car[iCar], rep[iRep], codi[iCod]);
    iCar++, iRep++, iCod++;
}
```

![](Imatges/Imatge5.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

<div id='id5' />

### Descompactació

Ara només queda fer la descompactació del text.
Per fer-ho, he tingut clar que el text compactat l’havia de tractar a nivell de paraula, en aquest cas, a nivell de codi. 
Per això, el primer que faig és obtenir cada codi i guardar-lo en una variable, per poder-lo comparar amb els codis del vector de codis. 
Li dic al programa que quan trobi un dollar el salti, i fins que no trobi un altre dollar o el \0 final, que copiï el codi a una variable. 

```c
 // Inicialitzacions
iTxt=0;
printf ("\nIntrodueix el text compactat: \n");
scanf ("%300[^\n]", textComp);
printf ("\nEl text descompactat es: \n");
// Mentre no final
while (textComp[iTxt] != '\0') {
    iCodTmp= 0;
    // Saltar dollars
    while (textComp[iTxt] == '$') iTxt++;
    // Copiar codi
    while (textComp[iTxt] != '$' && textComp[iTxt] != '\0') {
        codiTmp[iCodTmp]=textComp[iTxt];
        iTxt++;
        iCodTmp++;
    }
    codiTmp[iCodTmp]='\0';
```

Un cop copiat el codi, el comparo caràcter a caràcter amb cada codi del vector de codis, fins que troba el codi que és, i un cop trobat el codi, símplement imprimeix el caràcter assignat al codi corresponent. 

```c
    iCodTmp=0;
    iCod=0;
    iBit=0;
    // Comparar el codi copiat amb la resta de codis del vector codi
    while (codi[iCod][iBit] != '\0' || codiTmp[iCodTmp] != '\0') {

        if (codi[iCod][iBit] != codiTmp[iCodTmp]) {
            iBit=0;
            iCodTmp=0;
            iCod++;
        } else iBit++, iCodTmp++;
    }
    // Imprimir el caràcter assignat al codi corresponent
    if (codi[iCod][iBit] == '\0' && codiTmp[iCodTmp] == '\0'){
        printf ("%c", car[iCod]);
    }
} // Fi mentre no final
```

![](Imatges/Imatge3.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---
