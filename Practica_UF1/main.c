#include <stdio.h>
#include <stdlib.h>

#define BB while (getchar()!='\n')
#define MAX_TEXT 100
#define MAX_REP 100
#define MAX_CAR 100
#define MAX_CODIS 29
#define MAX_DIGITS 5
#define PERCENTATGE_COMPACTACIO ((((float)iTxt * 8) - (float)bit)/((float)iTxt*8)*100)
#define MAX_TEXT_COMP 300

int main()
{
    // Declaració de variables
    int iTxt, iCar, iRep, iCod, iBit, iCodTmp; // Index
    int i, max, aux; // Ordenar caràcters i repeticions
    int bit; // Numero de bits que hi ha al text compactat
    int rep[MAX_REP+1]; // Numero de cops que es repeteix cada caràcter
    char text[MAX_TEXT+1]; // Text a compactar, introduit per l'usuari
    char textComp[MAX_TEXT_COMP+1]; // Text compactat
    char car[MAX_CAR+1]; // Caràcters del text descompactat
    char auxCar; // Ordenar caràcters. Guarda momentàniament el caràcter per no perdre-l
    char codi[MAX_CODIS][ MAX_DIGITS+1]={ "0" , "1" , "00" , "01" , "10" , "11" , "000" ,
    "001" , "010" , "011" , "100" , "101" , "110" , "111" , "0000" , "0001" , "0010" ,
    "0011" , "0100" , "0101" , "0110" , "0111" , "1000" , "1001" , "1010" , "1100" , "1101" , "1110" , "1111" };
    char codiTmp[MAX_DIGITS+1]; // Descompactar. Es copia el codi en aquesta variable per poder-lo comparar amb els codis del vector codi.

    // COMPACTACIÓ

    // Inicialitzacions
    iTxt= 0;
    bit= 0;

    for (iRep=0; iRep < MAX_REP; iRep++) rep[iRep]=1; // Inicialització de les repeticions amb 1
    for (iCar=0; iCar < MAX_CAR; iCar++) car[iCar]='\0'; // Inicialització dels caràcters amb /0

    printf ("Introdueix el text a codificar: \n");
    scanf ("%100[^\n]", text); BB;

    // Mentre no final
    while (text[iTxt] != '\0') {

        iCar=0;
        iRep=0;

        // Mirar si el caràcter del text és al vector car
        while (car[iCar] != '\0' && car[iCar] != text[iTxt]) {
                iCar++;
                iRep++;
            }

        // Si no hi és, afegir-lo
        // Si hi és, sumar una repetició
        if (car[iCar] == '\0') {
            car[iCar]=text[iTxt];
            iCar++;
        } else rep[iRep]++;

        iTxt++;

    } // Fi mentre no final

    // Ordenar els caràcters del vector car per nombre de repeticions
    max= 0;

    while (car[max] != '\0') max++;

    for (i=2; i <= max; i++) {

        for (iRep=0 ; iRep <= max-i ; iRep++) {

            // Si el caràcter te menys repeticions que el següent, intercanviar-los
            if (rep[iRep] < rep[iRep+1]) {
                aux=rep[iRep];
                rep[iRep]=rep[iRep+1];
                rep[iRep+1]=aux;

                auxCar=car[iRep];
                car[iRep]=car[iRep+1];
                car[iRep+1]=auxCar;
            }
        }
    }

    iTxt=0;

    // Imprimir text compactat
    printf ("\nText compactat:\n");

    // Mentre no final
    while (text[iTxt] != '\0') {

        iCar=0;
        iCod=0;

        while (car[iCar] != '\0') {

            iBit=0;

            // Imprimir el codi del caràcter concret i anar sumant els bits
            if (car[iCar] == text[iTxt]) {

                if (iTxt != 0) printf ("$");

                while (codi[iCod][iBit] != '\0') {
                    printf ("%c", codi[iCod][iBit]);
                    iBit++, bit++;
                }

                break;

            } else iCar++, iCod++;
        }

        iTxt++;

    } // Fi mentre no final

    // Imprimir el percentatge de compactació
    printf ("\n\nBits del text: %ix8 = %i bits\n", iTxt, iTxt*8);
    printf ("Bits del text compactat: %i bits", bit);
    printf ("\nPercentatge de compactacio: %.2f%%", PERCENTATGE_COMPACTACIO);

    iCar=0;
    iRep=0;
    iCod=0;

    // Imprimir la taula de caràcters
    printf ("\n\nTaula (caracter - repeticions - codi assignat)\n");

    while (car[iCar] != '\0'){

        if (rep[iRep] == 1) printf ("'%c'\t'%i repeticio'\t'%s'\n", car[iCar], rep[iRep], codi[iCod]);
        else if (rep[iRep] >= 10) printf ("'%c'\t'%i repeticions''%s'\n", car[iCar], rep[iRep], codi[iCod]);
        else printf ("'%c'\t'%i repeticions'\t'%s'\n", car[iCar], rep[iRep], codi[iCod]);

        iCar++, iRep++, iCod++;
    }

    // DESCOMPACTACIÓ

    // Inicialitzacions
    iTxt=0;

    printf ("\nIntrodueix el text compactat: \n");
    scanf ("%300[^\n]", textComp);

    printf ("\nEl text descompactat es: \n");

    // Mentre no final
    while (textComp[iTxt] != '\0') {

        iCodTmp= 0;

        // Saltar dollars
        while (textComp[iTxt] == '$') iTxt++;

        // Copiar codi
        while (textComp[iTxt] != '$' && textComp[iTxt] != '\0') {
            codiTmp[iCodTmp]=textComp[iTxt];
            iTxt++;
            iCodTmp++;
        }

        codiTmp[iCodTmp]='\0';
        iCodTmp=0;
        iCod=0;
        iBit=0;

        // Comparar el codi copiat amb la resta de codis del vector codi
        while (codi[iCod][iBit] != '\0' || codiTmp[iCodTmp] != '\0') {

            if (codi[iCod][iBit] != codiTmp[iCodTmp]) {
                iBit=0;
                iCodTmp=0;
                iCod++;
            } else iBit++, iCodTmp++;
        }

        // Imprimir el caràcter assignat al codi corresponent
        if (codi[iCod][iBit] == '\0' && codiTmp[iCodTmp] == '\0'){
            printf ("%c", car[iCod]);
        }

    } // Fi mentre no final

    printf ("\n");

    return 0;
}
